package com.example.final11;

public class Employee {

    // Data member
    private String name;

    /**
     * No-arg constructor
     */
    public Employee() {
        name = "none";
    }

    /**
     * Parameterized Constructor
     * @param name, the employee's name as a String
     */
    public Employee(String name) {
        super();
        this.name = name;
    }

    /**
     * getName( ) method
     * Purpose: Get a new name
     @return the new name
     */
    public String getName() {
        return name;
    }

    /**
     * setName( ) method
     * Purpose: Set a new name
     @param name, new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * toString( ) method
     * Purpose: To create a String representation of the employee
     @return a string representation of the name
     */
    @Override
    public String toString() {
        return "<h2>Name= " + name + "</h2>";
    }
}