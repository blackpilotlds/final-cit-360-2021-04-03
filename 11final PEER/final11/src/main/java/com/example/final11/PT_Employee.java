package com.example.final11;

import java.text.DecimalFormat;

public class PT_Employee extends Employee {

    // Data members
    private int hours;
    private int hourlyWage;

    /**
     * Parameterized Constructor
     * @param name, the employee's name as a String
     * @param hours, the Part-time employee's hours int
     * @param payPerHour, the Part-time employee's wage as int
     */
    public PT_Employee(String name, int hours, int payPerHour) {
        super(name);
        this.hours = hours;
        this.hourlyWage = payPerHour;
    }

    // Setters and getters
    /**
     * getHours( ) method
     * Purpose: Get a new value for hours
     @return hours
     */
    public int getHours() {
        return hours;
    }

    /**
     * setHours( ) method
     * Purpose: Set a new value for hours
     @param hours, new value
     */
    public void setHours(int hours) {
        this.hours = hours;
    }

    /**
     * getHourlyWage( ) method
     * Purpose: Get a new value for hourlyWage
     @return hourlyWage
     */
    public int getHourlyWage() {
        return hourlyWage;
    }

    /**
     * setHourlyWage( ) method
     * Purpose: Set a new value for hourlyWage
     @param hourlyWage, new value
     */
    public void setHourlyWage(int hourlyWage) {
        this.hourlyWage = hourlyWage;
    }

    /**
     * getSalary( ) method
     * Purpose: computes the overall salary
     @return (hourlyWage * hours) * 52
     */
    public double getSalary() {
        return (hourlyWage * hours) * 52;
    }

    /**
     * The toString method
     * Purpose: To create a String representation of the Part-time employee
     * @return String object
     */
    @Override
    public String toString() {
        DecimalFormat format = new DecimalFormat("#.00");
        return "<h1>Part-Time Employee</h1><br>" + super.toString() + "<h3>Annual Income= " + format.format(getSalary()) + "</h3>" ;
    }
}