package com.example.final11;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(name = "HelloServlet", urlPatterns={"/HelloServlet"})
public class HelloServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");

        // Creates an ArrayList<Employee>
        ArrayList<Employee> arl = new ArrayList<>();

        // Declaring Employee Class Variables
        Employee empPT;

        String name = request.getParameter("name");
        int hours = Integer.parseInt(request.getParameter("hours"));
        int payPerHour = Integer.parseInt(request.getParameter("payPerHour"));

        // Creating an Instance of PTEmployee (Part-time) Class
        empPT = new PT_Employee(name, hours, payPerHour);

        // adding Part-time Instance data into an array list ArrayList<Employee>

        arl.add(empPT);

        for (Employee employee : arl) {
            out.print(employee);
            out.println();
        }

        out.println("</body></html>");
    }
}