<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome Page</title>
</head>
<body>
<header>
    <p>[#1-index.jsp]</p>
    <h1>Personal Application @ BYUI</h1>
    <h2>Sergei Aleinikov<br>
        Troy Tuckett<br>
        CIT 360-02 Object Oriented Development<br>
        31 March 2021<br>
    </h2>
    <p>This program simulates a payroll program,<br>
        and will display the annual pay<br>
        for the Part-time employee.</p>
</header>
<main>
    <p><b>Part-Time Employment</b> <a href="${pageContext.request.contextPath}/PT_Employee.html">PT_Payroll</a></p>
</main>
<footer></footer>
</body>
</html>