package com.example.final11;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PT_EmployeeTest {

    @Test
    void getName() {
        PT_Employee e = new PT_Employee("Sergei", 18,9);
        assertEquals("Sergei", e.getName());
    }
}