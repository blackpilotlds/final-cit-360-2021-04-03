package com.example.final11;

import java.text.DecimalFormat;

public class PT_Employee extends Employee {

    // Data members
    private int hours;
    private int hourlyWage;

    public PT_Employee(String name, int hours, int payPerHour) {
        super(name);
        this.hours = hours;
        this.hourlyWage = payPerHour;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(int hourlyWage) {
        this.hourlyWage = hourlyWage;
    }

    public double getSalary() {
        return (hourlyWage * hours) * 52;
    }

    @Override
    public String toString() {
        DecimalFormat format = new DecimalFormat("#.00");
        return "<h1>Part-Time Employee</h1><br>" + super.toString() + "<h3>Annual Income= " + format.format(getSalary()) + "</h3>" ;
    }
}