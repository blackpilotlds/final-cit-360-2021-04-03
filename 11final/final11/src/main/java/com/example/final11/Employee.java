package com.example.final11;

public class Employee {

    private String name;

    public Employee() {
        name = "none";
    }

    public Employee(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "<h2>Name= " + name + "</h2>";
    }
}